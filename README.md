# Some info

Proposed video -> Bumblebee

List is scrollable (horizontally)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

!!IMPORTANT - before use set base api url in root file ".env" in variable "REACT_APP_BASE_URL"

## Available Scripts

In the project directory, you can run:

### `npm start`
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`
### `npm run build`
### `npm run eject`
