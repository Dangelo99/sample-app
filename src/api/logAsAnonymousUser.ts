import React from 'react';
import { useDispatch } from 'react-redux';

import { AppDispatch } from '../storeRedux';
import { saveUser } from '../storeRedux/userSlice';
import { saveToken } from '../storeRedux/authorizationTokenSlice';
import { LOG_ANONYMOUS_USER_URL, BAD_RESPONSE_ERROR } from './static';

function triggerFetch() {
  return fetch(LOG_ANONYMOUS_USER_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Device: {
        PlatformCode: 'WEB',
        Name: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
      },
    }),
  })
    .then((response) => {
      if (response.status !== 200 || response.ok === false) {
        throw BAD_RESPONSE_ERROR;
      }
      return response;
    })
    .then((response) => response.json())
    .then((resp) => {
      return { user: resp.User, authorizationToken: resp.AuthorizationToken };
    });
}

function useLogAsAnonymousUser() {
  const [error, setError] = React.useState<boolean>(false);
  const dispatch = useDispatch<AppDispatch>();

  const mount = React.useRef<boolean>(true);

  React.useEffect(() => {
    return () => {
      mount.current = false;
    };
  }, []);

  const logAsAnonymousUser = React.useCallback(() => {
    triggerFetch().then(
      ({ user, authorizationToken }) => {
        if (mount.current) {
          dispatch(saveUser({ ...user, IsAnonymous: true }));
          dispatch(saveToken({ ...authorizationToken }));
        }
      },
      (err) => {
        if (mount.current) {
          setError(true);
        }
      }
    );
  }, [dispatch]);

  return { error, logAsAnonymousUser };
}

export { useLogAsAnonymousUser };
