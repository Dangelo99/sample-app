const API_BASE_URL = process.env.REACT_APP_BASE_URL;
const FETCH_VIDEOS_LIST_URL = `${API_BASE_URL}/Media/GetMediaList`;
const LOG_ANONYMOUS_USER_URL = `${API_BASE_URL}/Authorization/SignIn`;
const GET_MEDIA_PLAY_INFO_URL = `${API_BASE_URL}/Media/GetMediaPlayInfo`;

const BAD_RESPONSE_ERROR = new Error('Bad status code or response is not ok');

export {
  API_BASE_URL,
  FETCH_VIDEOS_LIST_URL,
  LOG_ANONYMOUS_USER_URL,
  GET_MEDIA_PLAY_INFO_URL,
  BAD_RESPONSE_ERROR,
};
