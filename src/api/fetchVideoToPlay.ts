import React from 'react';
import { useSelector } from 'react-redux';

import { selectToken } from '../storeRedux/authorizationTokenSlice';
import { GET_MEDIA_PLAY_INFO_URL, BAD_RESPONSE_ERROR } from './static';

export interface VideoData {
  MediaId: number;
  Title: string;
  Description: string;
  MediaTypeCode: string;
  MediaTypeDisplayName: string;
  StreamId: number;
  Provider: string;
  ContentUrl: string;
}

export type StreamType = 'TRIAL';

function triggerFetch(
  token: string,
  mediaId: number,
  streamType: StreamType
): Promise<VideoData> {
  return fetch(GET_MEDIA_PLAY_INFO_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify({
      MediaId: mediaId,
      StreamType: streamType,
    }),
  })
    .then((response) => {
      if (response.status !== 200 || response.ok === false) {
        throw BAD_RESPONSE_ERROR;
      }
      return response;
    })
    .then((response) => response.json());
}

function useFetchVideoToPlay() {
  const authorizationToken = useSelector(selectToken);
  const [error, setError] = React.useState<boolean>(false);
  const [videoData, setVideoData] = React.useState<VideoData | null>(null);

  const mount = React.useRef<boolean>(true);

  React.useEffect(() => {
    return () => {
      mount.current = false;
    };
  }, []);

  const fetchVideoToPlay = React.useCallback(
    (mediaId: number, streamType: StreamType) => {
      triggerFetch(authorizationToken.Token, mediaId, streamType).then(
        (videoDataFromApi) => {
          if (mount.current) {
            setVideoData(videoDataFromApi);
          }
        },
        (err) => {
          if (mount.current) {
            setError(true);
          }
        }
      );
    },
    [authorizationToken]
  );

  return { error, videoData, fetchVideoToPlay };
}

export { useFetchVideoToPlay };
