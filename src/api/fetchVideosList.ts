import React from 'react';
import { useSelector } from 'react-redux';

import { selectToken } from '../storeRedux/authorizationTokenSlice';
import { FETCH_VIDEOS_LIST_URL, BAD_RESPONSE_ERROR } from './static';

export interface VideoEntity {
  Id: number;
  Title: string;
  ImageUrl: string;
}

function extractVideoRelevantInfo(source: any[]): VideoEntity[] {
  const sourceWithIds = source.filter(
    (entity) => entity.Id && typeof entity.Id === 'number'
  );
  const output = sourceWithIds.map((entity) => {
    let imageUrl = '';
    if (entity.Images && Array.isArray(entity.Images)) {
      const entityImage = entity.Images.find(
        (el: any) => el.ImageTypeCode && el.ImageTypeCode === 'FRAME'
      );
      if (entityImage !== undefined && entityImage.Url) {
        imageUrl = entityImage.Url;
      }
    }
    return {
      Id: entity.Id,
      Title: entity.Title || 'Title unavailable',
      ImageUrl: imageUrl,
    };
  });
  return output;
}

function triggerFetch(token: string, mediaListId: number, pageNumber: number) {
  return fetch(FETCH_VIDEOS_LIST_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify({
      MediaListId: mediaListId,
      IncludeCategories: false,
      IncludeImages: true,
      IncludeMedia: false,
      PageNumber: pageNumber,
      PageSize: 15,
    }),
  })
    .then((response) => {
      if (response.status !== 200 || response.ok === false) {
        throw BAD_RESPONSE_ERROR;
      }
      return response;
    })
    .then((response) => response.json())
    .then((resp) => {
      if (resp.Entities && Array.isArray(resp.Entities)) {
        return extractVideoRelevantInfo(resp.Entities);
      } else {
        return [];
      }
    });
}

function useFetchVideosList() {
  const authorizationToken = useSelector(selectToken);
  const [error, setError] = React.useState<boolean>(false);
  const [videosList, setVideosList] = React.useState<VideoEntity[]>([]);

  const mount = React.useRef<boolean>(true);

  React.useEffect(() => {
    return () => {
      mount.current = false;
    };
  }, []);

  const fetchVideosList = React.useCallback(
    (mediaListId: number, pageNumber: number) => {
      triggerFetch(authorizationToken.Token, mediaListId, pageNumber).then(
        (videosEntities) => {
          if (mount.current) {
            setVideosList(videosEntities);
          }
        },
        (err) => {
          if (mount.current) {
            setError(true);
          }
        }
      );
    },
    [authorizationToken]
  );

  return { error, videosList, fetchVideosList };
}

export { useFetchVideosList };
