import React from 'react';

import { useHistory } from 'react-router';

import './Error.css';

export default function Error() {
  const [imageLoaded, setImageLoaded] = React.useState<boolean>(false);
  const history = useHistory();

  const handleClickGoHome = () => {
    history.replace('/');
  };

  return (
    <div className={'error-screen'}>
      <h2>Something went wrong</h2>
      <div className={'ec-image'}>
        <img
          src={'/errorScreenPopcorn.webp'}
          alt={':('}
          className={`${!imageLoaded ? 'hidden' : ''}`}
          onLoad={() => setImageLoaded(true)}
        />
      </div>
      <button onClick={handleClickGoHome}>GO TO MAIN PAGE</button>
    </div>
  );
}
