import React from 'react';
import { useHistory } from 'react-router';
import { useSelector } from 'react-redux';

import SplashScreen from './SplashScreen';
import Error from './Error';
import SpinnerDotLine from '../commonComponents/SpinnerDotLine';

import { useFetchVideosList, VideoEntity } from '../api/fetchVideosList';
import { useLogAsAnonymousUser } from '../api/logAsAnonymousUser';
import { selectUser, UserState } from '../storeRedux/userSlice';
import {
  selectToken,
  AuthorizationTokenState,
} from '../storeRedux/authorizationTokenSlice';

import './Home.css';

export default function Home() {
  const user: UserState = useSelector(selectUser);
  const authorizationToken: AuthorizationTokenState = useSelector(selectToken);
  const { error, logAsAnonymousUser } = useLogAsAnonymousUser();
  const [splashScreenShow, setSplashScreenShow] = React.useState<boolean>(true);

  React.useEffect(() => {
    if (user.Id === 0) {
      setSplashScreenShow(true);
      logAsAnonymousUser();
    } else {
      const splashScreenOff = setTimeout(() => {
        setSplashScreenShow(false);
      }, 2000);
      return () => {
        clearTimeout(splashScreenOff);
      };
    }
  }, [user, logAsAnonymousUser]);

  return (
    <>
      {error ? (
        <Error />
      ) : (
        <>
          <SplashScreen show={splashScreenShow} />
          {user.Id !== 0 && authorizationToken.Token !== '' && (
            <div className={`home-screen ${splashScreenShow ? 'hidden' : ''}`}>
              <div className={'header'}>
                <h1>HOME CINEMA</h1>
              </div>
              <VideosList mediaListId={2} />
              <VideosList mediaListId={3} />
            </div>
          )}
        </>
      )}
    </>
  );
}

function VideosList(props: { mediaListId: number }) {
  const { error, videosList, fetchVideosList } = useFetchVideosList();
  const [loading, setLoading] = React.useState<boolean>(false);
  const [pageNumber, setPageNumber] = React.useState<number>(1);

  React.useEffect(() => {
    setLoading(false);
  }, [videosList]);

  React.useEffect(() => {
    setLoading(true);
    fetchVideosList(props.mediaListId, pageNumber);
  }, [fetchVideosList, props.mediaListId, pageNumber]);

  return (
    <div className={'videos-list'}>
      <h2>Media list collection: {props.mediaListId}</h2>
      {error ? (
        <p>Sorry, resource unavailable</p>
      ) : loading ? (
        <p>fetching videos</p>
      ) : (
        <ul>
          {videosList.length !== 0 ? (
            videosList.map((videoEntity: VideoEntity) => (
              <VideoListItem key={videoEntity.Id} videoEntity={videoEntity} />
            ))
          ) : (
            <p>nothing to show</p>
          )}
        </ul>
      )}
    </div>
  );
}

function VideoListItem(props: { videoEntity: VideoEntity }) {
  const history = useHistory();

  const handleClickVideo = () => {
    history.push('/Player', {
      mediaId: props.videoEntity.Id,
      streamType: 'TRIAL',
    });
  };

  return (
    <li className={'vl-item'} onClick={handleClickVideo}>
      <ShowCover src={props.videoEntity.ImageUrl} />
      <h4>{props.videoEntity.Title}</h4>
    </li>
  );
}

function ShowCover(props: { src: string }) {
  const [imageLoaded, setImageLoaded] = React.useState<boolean>(false);

  return (
    <div className={'vli-cover'}>
      <img
        src={props.src || '/movieCover.webp'}
        alt={'Video cover'}
        className={`${!imageLoaded ? 'loading' : ''}`}
        onLoad={() => setImageLoaded(true)}
      />
      {!imageLoaded && <SpinnerDotLine />}
    </div>
  );
}
