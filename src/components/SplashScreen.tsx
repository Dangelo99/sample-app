import React from 'react';

import SpinnerDotLine from '../commonComponents/SpinnerDotLine';

import './SplashScreen.css';

function SplashScreen(props: { show: boolean }) {
  const [imageLoaded, setImageLoaded] = React.useState<boolean>(false);

  if (props.show === false) {
    return null;
  } else {
    return (
      <div className={'splash-screen'}>
        <div className={'sc-image'}>
          <img
            src={'/splashScreenPopcorn.webp'}
            alt={'Loading...'}
            className={`${!imageLoaded ? 'hidden' : ''}`}
            onLoad={() => setImageLoaded(true)}
          />
        </div>
        <SpinnerDotLine />
      </div>
    );
  }
}

const MemoizedSplashScreen = React.memo(SplashScreen);

export default MemoizedSplashScreen;
