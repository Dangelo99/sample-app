import React from 'react';
import { useHistory, useLocation } from 'react-router';
import ReactHlsPlayer from 'react-hls-player';

import Error from '../components/Error';
import { useFetchVideoToPlay, StreamType } from '../api/fetchVideoToPlay';

import './Player.css';

interface LocationTypePlayer {
  mediaId: number;
  streamType: StreamType;
}

export default function Player() {
  const { error, videoData, fetchVideoToPlay } = useFetchVideoToPlay();
  const history = useHistory();
  const location = useLocation<LocationTypePlayer>();

  const playerRef = React.useRef<HTMLVideoElement | null>(null);

  React.useEffect(() => {
    if (location.state.mediaId && location.state.streamType) {
      fetchVideoToPlay(location.state.mediaId, location.state.streamType);
    } else {
      history.replace('/');
    }
  }, [fetchVideoToPlay, location, history]);

  const handleGoToVideosList = () => {
    history.goBack();
  };

  return (
    <>
      {error ? (
        <Error />
      ) : (
        <>
          <button
            className={'player-go-back-btn'}
            onClick={handleGoToVideosList}>
            GO BACK TO VIDEOS LIST
          </button>
          {videoData !== null && (
            <>
              <h2>{videoData.Title}</h2>
              <ReactHlsPlayer
                playerRef={playerRef}
                src={videoData.ContentUrl}
                autoPlay={false}
                controls={true}
                width="70%"
                height="auto"
              />
            </>
          )}
        </>
      )}
    </>
  );
}
