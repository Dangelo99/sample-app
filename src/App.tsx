import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import store from './storeRedux';

import Home from './components/Home';
import Player from './components/Player';
import Error from './components/Error';

function App() {
  return (
    <Provider store={store}>
      <AppRouter />
    </Provider>
  );
}

function AppRouter() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/Player" exact component={Player} />
        <Route path="/Error" exact component={Error} />
        <Route component={Error} />
      </Switch>
    </Router>
  );
}

export default App;
