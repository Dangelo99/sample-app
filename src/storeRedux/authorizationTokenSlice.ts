import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from '.';

export interface AuthorizationTokenState {
  Token: string;
  TokenExpires: string;
}

const initialState: AuthorizationTokenState = {
  Token: '',
  TokenExpires: '',
};

export const authorizationTokenSlice = createSlice({
  name: 'authorizationToken',
  initialState,
  reducers: {
    saveToken: (state, action: PayloadAction<AuthorizationTokenState>) => {
      state.Token = action.payload.Token;
      state.TokenExpires = action.payload.TokenExpires;
    },
  },
});

export const { saveToken } = authorizationTokenSlice.actions;
export const selectToken = (state: RootState) => state.authorizationToken;

export default authorizationTokenSlice.reducer;
