import { configureStore } from '@reduxjs/toolkit';
import userSlice from './userSlice';
import authorizationTokenSlice from './authorizationTokenSlice';

const store = configureStore({
  reducer: {
    user: userSlice,
    authorizationToken: authorizationTokenSlice,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
