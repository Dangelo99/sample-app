import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from '.';

export interface UserState {
  Id: number;
  UserName: string;
  FullName: string;
  ClientRoles: any[];
  IsAnonymous: boolean;
}

const initialState: UserState = {
  Id: 0,
  UserName: '',
  FullName: '',
  ClientRoles: [],
  IsAnonymous: true,
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    saveUser: (state, action: PayloadAction<UserState>) => {
      state.Id = action.payload.Id;
      state.UserName = action.payload.UserName;
      state.FullName = action.payload.FullName;
      state.ClientRoles = action.payload.ClientRoles;
      state.IsAnonymous = action.payload.IsAnonymous;
    },
  },
});

export const { saveUser } = userSlice.actions;
export const selectUser = (state: RootState) => state.user;

export default userSlice.reducer;
