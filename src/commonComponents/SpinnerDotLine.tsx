import React from 'react';
import './SpinnerDotLine.css';

export default function SpinnerDotLine() {
  return (
    <div className="lds-ellipsis">
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
}
